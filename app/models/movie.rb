class Movie < ActiveRecord::Base
  attr_accessible :title, :rating, :description, :release_date
  def self.getRatedSorted(hsh, st)  	
  	hsh.select! { |x,y| y=='1' }  	
  	m = where({:rating => hsh.keys})
  	m = m.order(st) unless st.nil?
  	m
  end
end
