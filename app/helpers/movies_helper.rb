module MoviesHelper
	# Checks if a number is odd:
	def oddness(count)
		count.odd? ?  "odd" :  "even"
	end
  #Helper for hilite class
	def hlSelect(st)
		haml_tag :th, {:class => ('hilite' unless params[:sort] != st)}
	end
end
