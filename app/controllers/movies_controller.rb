class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index
  	@all_ratings = Movie.uniq.pluck(:rating)
  	@orig = params  	
  	@redir = false
	if params[:ratings].nil? or params[:ratings].values.count('0') == params[:ratings].size
		if not session[:ratings].nil?
			params[:ratings] = session[:ratings]
			@redir = true
		else
	  		#If no session, set params to default
	  		params[:ratings] = Hash.new
			@all_ratings.each { |x| params[:ratings][x]='1' }
		end
	else
		session[:ratings] = params[:ratings]
	end
	
  	if params[:sort].nil?
  		if not session[:sort].nil?  	
  			params[:sort] = session[:sort]
  			@redir = true
		end
  	else   		  		
		session[:sort] = params[:sort]	
  	end
  	
	if @redir
		flash.keep
		redirect_to :action => 'index', :ratings => params[:ratings], :sort => params[:sort]		
	end
  		
	@is_chk = params[:ratings] #for the view
	@movies = Movie.getRatedSorted(params[:ratings], params[:sort])
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
